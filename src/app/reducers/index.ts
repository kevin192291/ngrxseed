import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';

import { environment } from '../../environments/environment';
import { RouterStateUrl } from './router.reducer';

export interface State {
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>,
}

export const reducers: ActionReducerMap<State> = {
  routerReducer: fromRouter.routerReducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
