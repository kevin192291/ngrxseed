export interface IEnvironment {
    production: boolean,
    firebase: firebase,
}
export interface firebase {
    apiKey: string,
    authDomain: string,
    databaseURL: string,
    projectId: string,
    storageBucket: string,
    messagingSenderId: string
  }