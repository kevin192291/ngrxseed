import { IEnvironment } from './environment.interface';

export const environment: IEnvironment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyC8DxeL9E1S6O9UJwZ81xSrB8h6N0Lq6kI",
    authDomain: "rehab-c1606.firebaseapp.com",
    databaseURL: "https://rehab-c1606.firebaseio.com",
    projectId: "rehab-c1606",
    storageBucket: "rehab-c1606.appspot.com",
    messagingSenderId: "911795055334"
  }
};
