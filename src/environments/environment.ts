import { IEnvironment } from "./environment.interface";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment: IEnvironment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC8DxeL9E1S6O9UJwZ81xSrB8h6N0Lq6kI",
    authDomain: "rehab-c1606.firebaseapp.com",
    databaseURL: "https://rehab-c1606.firebaseio.com",
    projectId: "rehab-c1606",
    storageBucket: "rehab-c1606.appspot.com",
    messagingSenderId: "911795055334"
  }
};
